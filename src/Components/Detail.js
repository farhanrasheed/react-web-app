// Detail.js
import React, { useEffect, useState } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';

function Detail() {
  const { id } = useParams();
  const [item, setItem] = useState(null);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    // Fetch the specific item's details based on the 'id' param
    axios.get(`https://rickandmortyapi.com/api/character/${id}`)
      .then((response) => {
        setItem(response.data);
        setLoading(false);
      })
      .catch((error) => {
        console.error('Error fetching item details:', error);
        setLoading(false);
      });
  }, [id]);

  if (loading) {
    return <div>Loading...</div>;
  }

  if (!item) {
    return <div>Item not found</div>;
  }

  return (
    <div className='d-flex justify-content-center align-items-center vh-100'>

        <div className='text-center'>
            <h2>{item.name}</h2>
            <img src={item.image} style={{borderRadius: "5px"}} alt={item.name} />
            <p>Status: {item.status}</p>
            <p>Gender: {item.gender}</p>
        </div>
      
    </div>
  );
}

export default Detail;