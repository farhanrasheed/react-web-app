import React, { useEffect, useState } from 'react';
import axios from 'axios';
import {
    Link,
  } from "react-router-dom";

function Listing() {
    // alert('hi')
    const [items, setItems] = useState([]);
    const [loading, setLoading] = useState(true);

    useEffect(() => {
        axios.get('https://rickandmortyapi.com/api/character')
            .then((response) => {
                console.log(response)
                setItems(response.data.results);
                setLoading(false);
            })
            .catch((error) => {
                console.error('Error fetching data:', error);
                setLoading(false);
            });
    }, []);

    if (loading) {
        return <div>Loading...</div>;
    }

    return (
        <div>

            <div class="container table-responsive py-5">
                <table class="table table-bordered table-hover">
                    <thead class="thead-dark">
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Name</th>
                            <th scope="col">Gender</th>
                            <th scope="col">Status</th>
                            <th scope="col">Image</th>
                            <th scope="col">Detail</th>
                        </tr>
                    </thead>
                    <tbody>
                        {items.map((item) => (
                            <tr>
                                <th scope="row">{item.id}</th>
                                <td>{item.name} </td>
                                <td>{item.gender}</td>
                                <td>{item.status}</td>
                                <td> <img style={{ width: "60px", borderRadius: "5px" }} src={item.image} /> </td>
                                <td>
                                    <Link to={`/detail/${item.id}`}>
                                        <button className='btn btn-primary'>Show Detail</button>
                                    </Link>
                                </td>
                            </tr>

                        ))}
                    </tbody>
                </table>
            </div>

        </div>
    );
}

export default Listing;